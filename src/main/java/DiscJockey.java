package main.java;

public class DiscJockey{

    SongComponent songList;

    // newSongList contains every Song, song group (album or playlist),
    // and any Songs saved in SongGroups

    public DiscJockey(SongComponent newSongList){

        songList = newSongList;

    }

    // Calls the displayMediaInfo() on every Song
    // or SongGroup stored in songList

    public void getSongList(){

        songList.displayMediaInfo();

    }

}