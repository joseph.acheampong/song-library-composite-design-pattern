package main.java;

import java.util.ArrayList;
import java.util.Iterator;

public class Album extends SongComponent {

    // Contains any Songs or SongGroups that are added
    // to this ArrayList

    ArrayList songComponents = new ArrayList();

    String albumName;
    int albumReleaseYear;

    public Album(String newAlbumName, int newReleaseYear){

        albumName = newAlbumName;
        albumReleaseYear = newReleaseYear;

    }

    public String getAlbumName() { return albumName; }
    public int getAlbumReleaseYear() { return albumReleaseYear; }

    public void add(SongComponent newSongComponent) {

        songComponents.add(newSongComponent);

    }

    public void remove(SongComponent newSongComponent) {

        songComponents.remove(newSongComponent);

    }

    public SongComponent getComponent(int componentIndex) {

        return (SongComponent)songComponents.get(componentIndex);

    }

    public void displayMediaInfo(){

        System.out.println("\n🎶 " +"Album Name: " + getAlbumName() + " was released " +
                getAlbumReleaseYear() );
        System.out.println("Displaying Album Songs... \n");

        // Cycles through and prints any Songs or song group (album or playlist) added
        // to this SongGroups ArrayList songComponents

        Iterator songIterator = songComponents.iterator();

        while(songIterator.hasNext()) {

            SongComponent songInfo = (SongComponent) songIterator.next();

            songInfo.displayMediaInfo();

        }

    }


}