package main.java;

import java.util.ArrayList;
import java.util.Iterator;

public class Playlist extends SongComponent {

    // Contains any Songs or song group (album or playlist) that are added
    // to this ArrayList

    ArrayList songComponents = new ArrayList();

    String playlistName;

    String playlistOwner;

    public Playlist(String newPlaylistName, String newPlaylistOwner){

        playlistName = newPlaylistName;
        playlistOwner = newPlaylistOwner;

    }

    public String getPlaylistName() { return playlistName; }

    public String getPlaylistOwner() {
        return playlistOwner;
    }

    public void add(SongComponent newSongComponent) {

        songComponents.add(newSongComponent);

    }

    public void remove(SongComponent newSongComponent) {

        songComponents.remove(newSongComponent);

    }

    public SongComponent getComponent(int componentIndex) {

        return (SongComponent)songComponents.get(componentIndex);

    }

    public void displayMediaInfo(){

        System.out.println("🎼 " + "Playlist Name: " + getPlaylistName() + " belongs to " +
                getPlaylistOwner());
        System.out.println("Displaying Songs on Playlist ... \n");

        // Cycles through and prints any Songs or song group (album or playlist) added
        // to this SongGroups ArrayList songComponents

        Iterator songIterator = songComponents.iterator();

        while(songIterator.hasNext()) {

            SongComponent songInfo = (SongComponent) songIterator.next();

            songInfo.displayMediaInfo();

        }

    }


}