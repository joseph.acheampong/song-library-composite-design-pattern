package main.java;

public class Song extends SongComponent {

    String songName;
    String artistName;
    int releaseYear;

    public Song(String newSongName, String newArtistName, int newReleaseYear){

        songName = newSongName;
        artistName = newArtistName;
        releaseYear = newReleaseYear;

    }

    public String getSongName() { return songName; }
    public String getArtistName() { return artistName; }
    public int getReleaseYear() { return releaseYear; }

    public void displayMediaInfo(){

        System.out.println("\t🎵 " +getSongName() + " was recorded by " +
                getArtistName() + " in " + getReleaseYear());

    }

}