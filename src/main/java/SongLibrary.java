package main.java;

import java.util.ArrayList;
import java.util.Iterator;

public class SongLibrary extends SongComponent {

    // Contains any Songs or song group (album or playlist) that are added
    // to this ArrayList

    ArrayList songComponents = new ArrayList();

    String libraryName;
    String libraryDescription;

    public SongLibrary(String newLibraryName, String newLibraryDescription){

        libraryName = newLibraryName;
        libraryDescription = newLibraryDescription;

    }

    public String getLibraryName() { return libraryName; }
    public String getLibraryDescription() { return libraryDescription; }

    public void add(SongComponent newSongComponent) {

        songComponents.add(newSongComponent);

    }

    public void remove(SongComponent newSongComponent) {

        songComponents.remove(newSongComponent);

    }

    public SongComponent getComponent(int componentIndex) {

        return (SongComponent)songComponents.get(componentIndex);

    }

    public void displayMediaInfo(){

        System.out.println("📁" + getLibraryName() + " " +
                getLibraryDescription());
        System.out.println("Displaying All Media in Library ... \n\n");


        // Cycles through and prints any Songs or song group (album or playlist) added
        // to this SongGroups ArrayList songComponents

        Iterator songIterator = songComponents.iterator();

        while(songIterator.hasNext()) {

            SongComponent songInfo = (SongComponent) songIterator.next();

            songInfo.displayMediaInfo();

        }

    }


}