package main.java;

public class SongListGenerator {
    public static void main(String[] args){
        Album barIII =new Album("BAR III", 2014);
        Album villain = new Album("The Villain I Never Was", 2022);


        // Top level component that holds everything
        SongLibrary everySong = new SongLibrary("All Songs", "Every Song On Your Device");


        // This holds Songs plus an Album with Songs
        Playlist recentlyAdded = new Playlist("Highly Spiritual", "Mortey");

        everySong.add(recentlyAdded);
        recentlyAdded.add(new Song("Unstoppable", "Sia", 2016));
        recentlyAdded.add(new Song("Love On The Brain", "Rihanna", 2016));
        recentlyAdded.add(new Song("Promises", "Maverick City Music", 2020));

        recentlyAdded.add(villain);
        villain.add(new Song("Soja", "Black Sherif", 2022));
        villain.add(new Song("Kweku The Traveller", "Black Sherif", 2022));
        villain.add(new Song("Second Sermon Remix", "Black Sherif", 2021));




        // Composite that holds individual groups of songs
        // This is an Album that just holds Songs
        everySong.add(barIII);
        barIII.add(new Song("All Black", "EL ft Pappy Kojo and Joey B", 2014));
        barIII.add(new Song("16 Bars", "EL ft Gemini", 2014));


        DiscJockey djMorty = new DiscJockey(everySong);
        djMorty.getSongList();


    }
}
